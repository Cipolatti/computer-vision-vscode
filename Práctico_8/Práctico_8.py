# Práctico 8 de la materia visión por computadora
# Coloca una imagen dentro de 3 puntos seleccionados de otra imagen 
# Para esto necesita transformar la imagen a colocar en los 3 puntos

import cv2 as cv
import numpy as np
import math

ix, iy = -1, -1
ix_1, ix_2, iy_1, iy_2 = -1, -1, -1, -1
count = 0

# Función que se ejecuta al producirse un evento de mouse en la venta de imagen
def MouseEvent(event, x, y, flags, param):
    global ix, iy, ix_1, iy_1, ix_2, iy_2, count
    # Cuándo se presiona el botón 
    if event == cv.EVENT_LBUTTONDOWN:
        if count == 0:
            ix, iy = x, y
            # Dibuja un círculo en el punto clickeado
            cv.circle(img_in, (ix, iy), 4, (255, 0, 0), -1)
            count = count + 1

        elif count == 1:
            ix_1, iy_1 = x, y
            cv.circle(img_in, (ix_1, iy_1), 4, (255, 0, 0), -1)
            count = count + 1

        elif count == 2:
            ix_2, iy_2 = x, y
            cv.circle(img_in, (ix_2, iy_2), 4, (255, 0, 0), -1)
            count = 3

# Imagen donde se va a incrustar otra imagen
img_in = cv.imread('SRV.jpg')  
# Imagen a incrustrar
img_in_2 = cv.imread('Mate1.jpg')  
cv.namedWindow('image')
cv.setMouseCallback('image', MouseEvent)

while 1:
    cv.imshow('image', img_in)
    # Recién va a entrar al if, luego de que se hayan realizado los 3 clicks en la imagen
    if count == 3:
        # Arreglo con las coordenadas de los puntos realizados
        points = np.float32([[ix, iy], [ix_1, iy_1], [ix_2, iy_2]])

        # Arreglo con 3 esquinas de la imagen a incrustrar
        points_1 = np.float32([[0, 0], [img_in_2.shape[1], 0], [0, img_in_2.shape[0]]])

        # Calcula la matriz necesaria para transformar la imagen a incrustrar, tal que 
        # entre dentro del lugar que se seleccionó en la primer imagen
        M = cv.getAffineTransform(points_1, points)
        # 
        img_in_2_mod = cv.warpAffine(img_in_2, M, (img_in.shape[1], img_in.shape[0]))

        # Pasa la imagen modificada a escala de grises
        img2gray = cv.cvtColor(img_in_2_mod, cv.COLOR_BGR2GRAY)
        # Realiza un umbralizado 
        ret, mask = cv.threshold(img2gray, 3, 255, cv.THRESH_BINARY)
        #cv.imshow('máscara original', mask)
        # Realiza una NOT lógica a la máscara obtenida bit a bit
        mask_inv = cv.bitwise_not(mask)
        #cv.imshow('máscara invertida', mask_inv)
        # Realiza una AND lógica bit a bit 
        img1 = cv.bitwise_and(img_in,img_in, mask=mask_inv)
        #cv.imshow('Imagen original con máscara invertida', img1)
        img2 = cv.bitwise_and(img_in_2_mod, img_in_2_mod, mask=mask)
        #cv.imshow('Imagen a incrustrar con máscara', img2)
        # Combina las dos imágenes individuales
        inc = cv.add(img1, img2)

        cv.imshow('final', inc)
        count = 0
    if cv.waitKey(20) & 0xFF == 27:
        break

cv.destroyAllWindows()
