# Práctico número 2 de visión por computadora"

import cv2

# Valor de umbral el cuál a partir de él pone el pixel en negro 
umbral = 170       
# 255 = píxel negro                                      
max_value = 255   
# 0 = píxel blanco                                       
min_value = 0                       

# Lee la imagen y la guarda en escala de grises
img = cv2.imread('LesPaul.png', 0)        

# Estos dos for recorren todos los píxeles de la imagen y verifica si el valor es mayor o menos a un umbral
for i, row in enumerate(img):
    for j, col in enumerate(row):                        
        if img[i][j] >= umbral:
            img[i][j] = max_value
        else:
            img[i][j] = min_value
        
cv2.imwrite('imagenResultante1.png', img)
# Muestra la imagen en una ventana nueva
cv2.imshow('imagenResultante1', img)                     
cv2.waitKey(0)

