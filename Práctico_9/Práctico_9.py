# Práctico 9 de la materia visión por computadora
# Realizar una homografía
# Seleccionar los puntos en sentido horario, comenzando con la esquina superior izquierda.

import cv2 as cv
import numpy as np
import math

ix, iy = -1, -1
ix_1, ix_2, iy_1, iy_2, ix_3, iy_3 = -1, -1, -1, -1, -1, -1
count = 0


def MouseEvent(event, x, y, flags, param):
    global ix, iy, ix_1, iy_1, ix_2, iy_2, count, ix_3, iy_3
    if event == cv.EVENT_LBUTTONDOWN:
        if count == 0:
            ix, iy = x, y
            cv.circle(img_in, (ix, iy), 4, (255, 0, 0), -1)
            count = count + 1

        elif count == 1:
            ix_1, iy_1 = x, y
            cv.circle(img_in, (ix_1, iy_1), 4, (255, 0, 0), -1)
            count = count + 1

        elif count == 2:
            ix_2, iy_2 = x, y
            cv.circle(img_in, (ix_2, iy_2), 4, (255, 0, 0), -1)
            count = count + 1

        elif count == 3:
            ix_3, iy_3 = x, y
            cv.circle(img_in, (ix_3, iy_3), 4, (255, 0, 0), -1)
            count = 4

# Imagen a rectificar
img_in = cv.imread('Guitarra.jpg')          
cv.namedWindow('image')
cv.setMouseCallback('image', MouseEvent)

while 1:

    cv.imshow('image', img_in)
    if count == 4:
        # Arreglo de puntos marcados en la imagen    
        vertices_img_in = np.float32(
            [[ix, iy],
            [ix_1, iy_1],
            [ix_2, iy_2],
            [ix_3, iy_3]])

        # Arreglo de puntos finales. Ubicación final de los puntos marcados anteriormente.
        vertices_img_out = np.float32(
            [
                [0, 0],
                [img_in.shape[1], 0],
                [img_in.shape[1], img_in.shape[0]],
                [0, img_in.shape[0]]
            ])

        # Obtiene la matriz necesaria para transformar los puntos de vertices_img_in a vertices_img_out
        M = cv.getPerspectiveTransform(vertices_img_in, vertices_img_out)
        # Aplica la matriz obtenida 
        img_out_mod = cv.warpPerspective(
            img_in, M, (img_in.shape[1], img_in.shape[0]))
        cv.imshow('Imagen rectificada', img_out_mod)
        count = 0

    # 27 => Tecla escape
    if cv.waitKey(20) & 0xFF == 27:
        break

cv.destroyAllWindows()
