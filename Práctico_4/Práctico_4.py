# Práctico número 4 de visión por computadora - 
# Obtener el ancho y alto de las imágenes capturadas usando los macros de OpenCV

import cv2

fps = 30
# Abre la cámara
cap = cv2.VideoCapture(0)
# Código de 4 bytes para comprimir el video.
fourcc = cv2.VideoWriter_fourcc('X', 'V', 'I', 'D')
# obtiene el CAP_PROP_FRAME_WIDTH
width = int(cap.get(3))    
# Obtiene el CAP_PROP_FRAME_HEIGTH                                                
heigth = int(cap.get(4))                                                    
print('My webcam width is: ', width, 'and heigth is: ', heigth)
# Guarda el video con el nombre "output.mp4"
out = cv2.VideoWriter('output.mp4', fourcc, fps,(width, heigth))

while(cap.isOpened()):
    # Va capturando imágenes del video
    ret, frame = cap.read()
    if ret is True:
        out.write(frame)
        cv2.imshow('frame', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    else:
        break

cap.release()
out.release()
cv2.destroyAllWindows()
