# Práctico 5 de visión por computadora
# Recorta un rectángulo de una imagen y lo guarda en otra imagen

import cv2
import numpy as np

drawing = False
ix, iy = -1, -1

# Función que se llama cuándo ocurre algún evento proveniente del mouse
def draw(event, x, y, flags, param):    
    global ix, iy, drawing, mode, crop_img
    # Cuándo se aprieta el botón guarda las coordenadas (en píxeles) de donde se pulsó el click
    if event == cv2.EVENT_LBUTTONDOWN:    
        drawing = True
        ix, iy = x, y
    # Cuándo se aprieta el botón guarda las coordenadas (en píxeles) de donde se pulsó el click
    elif event == cv2.EVENT_LBUTTONUP:    
        drawing = False
        cv2.rectangle(imagen_entrante, (ix, iy), (x, y), (255, 0, 0), 3)
        crop_img = imagen_entrante[iy:y, ix:x]

# Lee la imagen
imagen_entrante = cv2.imread("LesPaul.png", 1)    

cv2.namedWindow("image")
# Cada vez que ocurra un evento del mouse en la ventana "image", llama a la función draw 
cv2.setMouseCallback("image", draw)

while 1:

    cv2.imshow("image", imagen_entrante)
    k = cv2.waitKey(1) & 0xFF

    # Si se presiona 'q' cierra el programa
    if k == ord("q"):    
        break
    # Si se presiona 'g', guarda la imagen recortada y cierra el programa
    elif k == ord("g"):    
        if not cv2.imwrite("Imagen_cortada.png", crop_img):
            print("No se pudo escribir la imagen")
        else:
            print("Se ha guardado la sección de la imagen marcada")
            break
    # Si se presiona 'r', restaura la imagen        
    elif k == ord("r"):    
        imagen_entrante = cv2.imread("LesPaul.png", 1)

cv2.destroyAllWindows()
