# IMPORTANT LINK :
# https://www.learnopencv.com/invisibility-cloak-using-color-detection-and-segmentation-with-opencv/

import cv2
import numpy

img = cv2.imread('imagen_práctico_10.png')


# converting from BGR to HSV color space
hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)

# Range for upper range
lower_red = numpy.array([170,120,70])
upper_red = numpy.array([190,255,255])
mask1 = cv2.inRange(hsv,lower_red,upper_red)

lower_blue = numpy.array([90, 40, 40])
upper_blue = numpy.array([120, 255, 255])
mask2 = cv2.inRange(hsv, lower_blue, upper_blue)

mask = mask1 + mask2
#Segmenting the cloth out of the frame using bitwise and with the inverted mask
res1 = cv2.bitwise_and(img,img,mask=mask)
gray = cv2.cvtColor(res1, cv2.COLOR_BGR2GRAY)
filtered = cv2.GaussianBlur(gray, (7, 7), 3)
ret, tresh = cv2.threshold(filtered, 70, 255, cv2.THRESH_BINARY)
edged = cv2.Canny(tresh, 70, 200)
contours, hierarchy = cv2.findContours(edged, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
for x, c in enumerate(contours):
    if x == 3:
        # Find max right point of the rubber
        right_point = tuple(c[c[:, :, 0].argmax()][0])
        # Find max point in Y of the rubber
        upper_point = tuple(c[c[: ,: , 1].argmax()][0])
        # Finde min point in Y of the rubber
        lower_point = tuple(c[c[:, :, 1].argmin()][0])
    if x == 4:
        # Find min left point of the rubber
        left_point = tuple(c[c[:, :, 0].argmin()][0])
x_size = right_point[0] - left_point[0]
y_size = upper_point[1] - lower_point[1]

rubber_size = (x_size, y_size)
# Contorno 3 = Parte azul de la goma
# Contorno 4 = Parte roja de la goma
cv2.drawContours(img, contours, 4, (0, 0, 255), 2)
cv2.imshow('contours', img)
cv2.imshow('test1', tresh)
cv2.waitKey(0)
