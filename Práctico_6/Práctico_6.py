# Práctico número 6 de la materia visión por computadora
# Realiza una transformación Euclidiana ( traslación + rotación)

import cv2 as cv
import numpy as np
import math


def Transform_image(img, angle, x, y, center=None):
    # Transforma el ángulo de grados a radianes 
    angle = np.radians(angle)

    # Arma la matriz de transformación
    Matriz = np.float32(
        [
            [math.cos(angle), math.sin(angle), x],
            [-math.sin(angle), math.cos(angle), y]
        ])

    print(Matriz)
    # Aplica la matriz de transformación a la imagen original 
    imagen = cv.warpAffine(img, Matriz, (1920, 1080))
    return imagen

# Leo la imagen original
img = cv.imread('Mate1.jpg')

while True:
    # Shows the original image
    cv.imshow('Original', img)          
    
    transf_img = Transform_image(img, 45, 500, 700)
    cv.imwrite('Output.jpg', transf_img)
    cv.imshow('Transformed image', transf_img)
    # El 0xFF es para guardar los últimos 8 bits.
    # Key es igual al resultado de una AND entre lo que retorna waitKey() y 0b11111111.
    key = cv.waitKey(20) & 0xFF

    if key == ord('q'):
        cv.destroyAllWindows()
        break

cv.destroyAllWindows()
