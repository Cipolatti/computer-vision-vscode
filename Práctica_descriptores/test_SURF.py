# Test de la función SURF

import cv2

img = cv2.imread('imagen_práctico_10.png')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

surf = cv2.xfeatures2d.SURF_create()
kp, dscr = surf.detectAndCompute(gray, None)

cv2.drawKeypoints(img, kp, img)

cv2.imwrite('surf_keypoints.png', img)
cv2.imshow('surf_keypoints', img)
cv2.waitKey()
