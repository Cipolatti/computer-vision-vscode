# Test para probar SIFT

import cv2

img = cv2.imread('boston_dyn.jpg')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

sift = cv2.xfeatures2d.SIFT_create()
kp, dscr = sift.detectAndCompute(gray, None)

cv2.drawKeypoints(img, kp, img)
cv2.imwrite('sift_keypoints.png', img)

cv2.imshow('sift_keypoints', img)
cv2.waitKey()
