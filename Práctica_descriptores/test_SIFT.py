# Example of SIFT. Finding features in an imagen
# Cause SIFT and SURF are patent, are not included in OpenCV
# To access nonfree detectores i've used: pip install opencv-contrib-python-nonfree (or pip3 in my case)
# This solution was taken from: https://stackoverflow.com/questions/52305578/sift-cv2-xfeatures2d-sift-create-not-working-even-though-have-contrib-instal


import cv2
import numpy as np

img = cv2.imread('imagen_práctico_10.png')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

sift = cv2.xfeatures2d.SIFT_create()
kp, dscr = sift.detectAndCompute(gray, None)

cv2.drawKeypoints(img, kp, img)

cv2.imwrite('sift_keypoints.png', img)

cv2.imshow('sift_keypoints', img)
cv2.waitKey()
