# Aplicación del detector FAST

import cv2
import numpy as np

img = cv2.imread('imagen_práctico_10.png')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

fast = cv2.FastFeatureDetector_create(threshold=50)
kp = fast.detect(gray, None)
cv2.drawKeypoints(img, kp, img, color=(255, 0, 0))

cv2.imwrite('fast_keypoints.png', img)
cv2.imshow('fast_keypoints', img)

cv2.waitKey()
